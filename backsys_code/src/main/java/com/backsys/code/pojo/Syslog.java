package com.backsys.code.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="syslog")
public class Syslog implements Serializable{

	@Id
	private String id;//


	@Column(name = "visittime")
	private Date visitTime;//
	@Column(name = "username")
	private String username;//
	@Column(name = "ip")
	private String ip;//
	@Column(name = "url")
	private String url;//
	@Column(name = "executiontime")
	private Integer executionTime;//
	@Column(name = "method")
	private String method;//


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Date getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(Integer executionTime) {
		this.executionTime = executionTime;
	}

	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}



}
