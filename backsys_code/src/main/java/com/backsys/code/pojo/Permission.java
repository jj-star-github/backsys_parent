package com.backsys.code.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="permission")
public class Permission implements Serializable{

	@Id
	private String id;//


	
	private String parentId;//
	private String permissionName;//
	private String url;//
	private String label;//

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {		
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getPermissionName() {		
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getUrl() {		
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getLabel() {		
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}


	
}
