package com.backsys.code.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="product")
public class Product implements Serializable{

	@Id
	private String id;//



	private String productNum;//
	private String productName;//
	private String cityName;//
	private Date DepartureTime;//
	private Double productPrice;//
	private String productDesc;//
	private Integer productStatus;//


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Date getDepartureTime() {
		return DepartureTime;
	}
	public void setDepartureTime(Date DepartureTime) {
		this.DepartureTime = DepartureTime;
	}

	public Double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductDesc() {
		return productDesc;
	}
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public Integer getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(Integer productStatus) {
		this.productStatus = productStatus;
	}



}
