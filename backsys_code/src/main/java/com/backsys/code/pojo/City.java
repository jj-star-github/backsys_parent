package com.backsys.code.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="city")
@ApiModel(value = "查询城市信息")
public class City implements Serializable{

	@ApiModelProperty(value = "城市的id")
	@Id
	private String id;//


	@ApiModelProperty(value = "城市的name")
	private String name;//


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



}
