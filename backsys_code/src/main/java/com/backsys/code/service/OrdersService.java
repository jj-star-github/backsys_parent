package com.backsys.code.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.backsys.code.dao.OrdersDao;
import com.backsys.code.pojo.Orders;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class OrdersService {

	@Autowired
	private OrdersDao ordersDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Orders> findAll() {
		return ordersDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Orders> findSearch(Map whereMap, int page, int size) {
		Specification<Orders> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return ordersDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Orders> findSearch(Map whereMap) {
		Specification<Orders> specification = createSpecification(whereMap);
		return ordersDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Orders findById(String id) {
		return ordersDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param orders
	 */
	public void add(Orders orders) {
		orders.setId( idWorker.nextId()+"" );
		ordersDao.save(orders);
	}

	/**
	 * 修改
	 * @param orders
	 */
	public void update(Orders orders) {
		ordersDao.save(orders);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		ordersDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Orders> createSpecification(Map searchMap) {

		return new Specification<Orders>() {

			@Override
			public Predicate toPredicate(Root<Orders> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("orderNum")!=null && !"".equals(searchMap.get("orderNum"))) {
                	predicateList.add(cb.like(root.get("orderNum").as(String.class), "%"+(String)searchMap.get("orderNum")+"%"));
                }
                // 
                if (searchMap.get("orderDesc")!=null && !"".equals(searchMap.get("orderDesc"))) {
                	predicateList.add(cb.like(root.get("orderDesc").as(String.class), "%"+(String)searchMap.get("orderDesc")+"%"));
                }
                // 
                if (searchMap.get("productId")!=null && !"".equals(searchMap.get("productId"))) {
                	predicateList.add(cb.like(root.get("productId").as(String.class), "%"+(String)searchMap.get("productId")+"%"));
                }
                // 
                if (searchMap.get("memberId")!=null && !"".equals(searchMap.get("memberId"))) {
                	predicateList.add(cb.like(root.get("memberId").as(String.class), "%"+(String)searchMap.get("memberId")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
