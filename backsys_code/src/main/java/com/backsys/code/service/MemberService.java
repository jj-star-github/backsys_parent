package com.backsys.code.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.backsys.code.dao.MemberDao;
import com.backsys.code.pojo.Member;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class MemberService {

	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Member> findAll() {
		return memberDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Member> findSearch(Map whereMap, int page, int size) {
		Specification<Member> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return memberDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Member> findSearch(Map whereMap) {
		Specification<Member> specification = createSpecification(whereMap);
		return memberDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Member findById(String id) {
		return memberDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param member
	 */
	public void add(Member member) {
		member.setId( idWorker.nextId()+"" );
		memberDao.save(member);
	}

	/**
	 * 修改
	 * @param member
	 */
	public void update(Member member) {
		memberDao.save(member);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		memberDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Member> createSpecification(Map searchMap) {

		return new Specification<Member>() {

			@Override
			public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("name")!=null && !"".equals(searchMap.get("name"))) {
                	predicateList.add(cb.like(root.get("name").as(String.class), "%"+(String)searchMap.get("name")+"%"));
                }
                // 
                if (searchMap.get("nickname")!=null && !"".equals(searchMap.get("nickname"))) {
                	predicateList.add(cb.like(root.get("nickname").as(String.class), "%"+(String)searchMap.get("nickname")+"%"));
                }
                // 
                if (searchMap.get("phoneNum")!=null && !"".equals(searchMap.get("phoneNum"))) {
                	predicateList.add(cb.like(root.get("phoneNum").as(String.class), "%"+(String)searchMap.get("phoneNum")+"%"));
                }
                // 
                if (searchMap.get("email")!=null && !"".equals(searchMap.get("email"))) {
                	predicateList.add(cb.like(root.get("email").as(String.class), "%"+(String)searchMap.get("email")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
