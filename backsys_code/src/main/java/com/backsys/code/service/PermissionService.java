package com.backsys.code.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.backsys.code.dao.PermissionDao;
import com.backsys.code.pojo.Permission;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class PermissionService {

	@Autowired
	private PermissionDao permissionDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Permission> findAll() {
		return permissionDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Permission> findSearch(Map whereMap, int page, int size) {
		Specification<Permission> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return permissionDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Permission> findSearch(Map whereMap) {
		Specification<Permission> specification = createSpecification(whereMap);
		return permissionDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Permission findById(String id) {
		return permissionDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param permission
	 */
	public void add(Permission permission) {
		permission.setId( idWorker.nextId()+"" );
		permissionDao.save(permission);
	}

	/**
	 * 修改
	 * @param permission
	 */
	public void update(Permission permission) {
		permissionDao.save(permission);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		permissionDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Permission> createSpecification(Map searchMap) {

		return new Specification<Permission>() {

			@Override
			public Predicate toPredicate(Root<Permission> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("parentId")!=null && !"".equals(searchMap.get("parentId"))) {
                	predicateList.add(cb.like(root.get("parentId").as(String.class), "%"+(String)searchMap.get("parentId")+"%"));
                }
                // 
                if (searchMap.get("permissionName")!=null && !"".equals(searchMap.get("permissionName"))) {
                	predicateList.add(cb.like(root.get("permissionName").as(String.class), "%"+(String)searchMap.get("permissionName")+"%"));
                }
                // 
                if (searchMap.get("url")!=null && !"".equals(searchMap.get("url"))) {
                	predicateList.add(cb.like(root.get("url").as(String.class), "%"+(String)searchMap.get("url")+"%"));
                }
                // 
                if (searchMap.get("label")!=null && !"".equals(searchMap.get("label"))) {
                	predicateList.add(cb.like(root.get("label").as(String.class), "%"+(String)searchMap.get("label")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
