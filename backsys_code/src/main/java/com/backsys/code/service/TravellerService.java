package com.backsys.code.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.backsys.code.dao.TravellerDao;
import com.backsys.code.pojo.Traveller;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class TravellerService {

	@Autowired
	private TravellerDao travellerDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Traveller> findAll() {
		return travellerDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Traveller> findSearch(Map whereMap, int page, int size) {
		Specification<Traveller> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return travellerDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Traveller> findSearch(Map whereMap) {
		Specification<Traveller> specification = createSpecification(whereMap);
		return travellerDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Traveller findById(String id) {
		return travellerDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param traveller
	 */
	public void add(Traveller traveller) {
		traveller.setId( idWorker.nextId()+"" );
		travellerDao.save(traveller);
	}

	/**
	 * 修改
	 * @param traveller
	 */
	public void update(Traveller traveller) {
		travellerDao.save(traveller);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		travellerDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Traveller> createSpecification(Map searchMap) {

		return new Specification<Traveller>() {

			@Override
			public Predicate toPredicate(Root<Traveller> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("name")!=null && !"".equals(searchMap.get("name"))) {
                	predicateList.add(cb.like(root.get("name").as(String.class), "%"+(String)searchMap.get("name")+"%"));
                }
                // 
                if (searchMap.get("sex")!=null && !"".equals(searchMap.get("sex"))) {
                	predicateList.add(cb.like(root.get("sex").as(String.class), "%"+(String)searchMap.get("sex")+"%"));
                }
                // 
                if (searchMap.get("phoneNum")!=null && !"".equals(searchMap.get("phoneNum"))) {
                	predicateList.add(cb.like(root.get("phoneNum").as(String.class), "%"+(String)searchMap.get("phoneNum")+"%"));
                }
                // 
                if (searchMap.get("credentialsNum")!=null && !"".equals(searchMap.get("credentialsNum"))) {
                	predicateList.add(cb.like(root.get("credentialsNum").as(String.class), "%"+(String)searchMap.get("credentialsNum")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
