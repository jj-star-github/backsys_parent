package com.backsys.code.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.backsys.code.dao.ProductDao;
import com.backsys.code.pojo.Product;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ProductService {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Product> findAll() {
		return productDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Product> findSearch(Map whereMap, int page, int size) {
		Specification<Product> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return productDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Product> findSearch(Map whereMap) {
		Specification<Product> specification = createSpecification(whereMap);
		return productDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Product findById(String id) {
		return productDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param product
	 */
	public void add(Product product) {
		product.setId( idWorker.nextId()+"" );
		productDao.save(product);
	}

	/**
	 * 修改
	 * @param product
	 */
	public void update(Product product) {
		productDao.save(product);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		productDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Product> createSpecification(Map searchMap) {

		return new Specification<Product>() {

			@Override
			public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("productNum")!=null && !"".equals(searchMap.get("productNum"))) {
                	predicateList.add(cb.like(root.get("productNum").as(String.class), "%"+(String)searchMap.get("productNum")+"%"));
                }
                // 
                if (searchMap.get("productName")!=null && !"".equals(searchMap.get("productName"))) {
                	predicateList.add(cb.like(root.get("productName").as(String.class), "%"+(String)searchMap.get("productName")+"%"));
                }
                // 
                if (searchMap.get("cityName")!=null && !"".equals(searchMap.get("cityName"))) {
                	predicateList.add(cb.like(root.get("cityName").as(String.class), "%"+(String)searchMap.get("cityName")+"%"));
                }
                // 
                if (searchMap.get("productDesc")!=null && !"".equals(searchMap.get("productDesc"))) {
                	predicateList.add(cb.like(root.get("productDesc").as(String.class), "%"+(String)searchMap.get("productDesc")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
