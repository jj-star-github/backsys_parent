package com.backsys.code.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.backsys.code.dao.SyslogDao;
import com.backsys.code.pojo.Syslog;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class SyslogService {

	@Autowired
	private SyslogDao syslogDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Syslog> findAll() {
		return syslogDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Syslog> findSearch(Map whereMap, int page, int size) {
		Specification<Syslog> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return syslogDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Syslog> findSearch(Map whereMap) {
		Specification<Syslog> specification = createSpecification(whereMap);
		return syslogDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Syslog findById(String id) {
		return syslogDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param syslog
	 */
	public void add(Syslog syslog) {
		syslog.setId( idWorker.nextId()+"" );
		syslogDao.save(syslog);
	}

	/**
	 * 修改
	 * @param syslog
	 */
	public void update(Syslog syslog) {
		syslogDao.save(syslog);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		syslogDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Syslog> createSpecification(Map searchMap) {

		return new Specification<Syslog>() {

			@Override
			public Predicate toPredicate(Root<Syslog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("username")!=null && !"".equals(searchMap.get("username"))) {
                	predicateList.add(cb.like(root.get("username").as(String.class), "%"+(String)searchMap.get("username")+"%"));
                }
                // 
                if (searchMap.get("ip")!=null && !"".equals(searchMap.get("ip"))) {
                	predicateList.add(cb.like(root.get("ip").as(String.class), "%"+(String)searchMap.get("ip")+"%"));
                }
                // 
                if (searchMap.get("url")!=null && !"".equals(searchMap.get("url"))) {
                	predicateList.add(cb.like(root.get("url").as(String.class), "%"+(String)searchMap.get("url")+"%"));
                }
                // 
                if (searchMap.get("method")!=null && !"".equals(searchMap.get("method"))) {
                	predicateList.add(cb.like(root.get("method").as(String.class), "%"+(String)searchMap.get("method")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
