package com.backsys.code.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.backsys.code.pojo.Member;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface MemberDao extends JpaRepository<Member,String>,JpaSpecificationExecutor<Member>{
	
}
