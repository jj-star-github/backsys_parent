package com.backsys.code.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.backsys.code.pojo.Users;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface UsersDao extends JpaRepository<Users,String>,JpaSpecificationExecutor<Users>{
	
}
