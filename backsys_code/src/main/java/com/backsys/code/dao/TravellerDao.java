package com.backsys.code.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.backsys.code.pojo.Traveller;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface TravellerDao extends JpaRepository<Traveller,String>,JpaSpecificationExecutor<Traveller>{
	
}
