package com.backsys.code.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.backsys.code.pojo.Permission;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface PermissionDao extends JpaRepository<Permission,String>,JpaSpecificationExecutor<Permission>{
	
}
